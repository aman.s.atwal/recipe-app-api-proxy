# Recipe App API Proxy

NGINX proxy app for recipe app API

## Environnment Variables

* `LISTEN_PORT` - Port to listen on (default: `8080`)
* `APP_HOST` - Hostname of the app to forard requests to (default: `app`)
* `APP_PORT` - Port of the app to forard requests to (default: `9000`)
